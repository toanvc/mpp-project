package group6.mpp;
import group6.mpp.objects.Member;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class UserController {

	@FXML
	private TextField userSearchField;

	// for 	the right grid Pane

	@FXML
	private GridPane gridPane;

	@FXML
	private TextField idLabel;
	@FXML
	private TextField nameLabel;
	@FXML
	private TextField roleLabel;


	// The table on the right
	@FXML
	private TableView<Member> userTable;
	@FXML
	private TableColumn<Member, String> idUserCol;

	@FXML
	private TableColumn<Member, String> nameUserCol;

	@FXML
	private Button updateUserButton;
	@FXML
	private Button delUserButton;
	@FXML
	private Button addUserButton;


}
