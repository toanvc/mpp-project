package group6.mpp.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class MyUtils {
	public static StringProperty convertDateToString(Date date) {
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		String fr = df.format(date);
		StringProperty s = new SimpleStringProperty(fr);
		return s;
	}
}
