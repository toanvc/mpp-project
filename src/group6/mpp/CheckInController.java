package group6.mpp;

import group6.mpp.objects.Member;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

public class CheckInController {


	@FXML
	private TextField searchField;


	@FXML
	private TextField isbnField;
	@FXML
	private TextField titleField;
	@FXML
	private TextField authorField;
	@FXML
	private TextField availableField;

	@FXML
	private TableView<Member> statusTable;

	@FXML
	private TableColumn<Member, String> idMemeberCol;
	@FXML
	private TableColumn<Member, String> fromMemberCol;
	@FXML
	private TableColumn<Member, String> toMemberCol;

	@FXML
	private Button checkinButton;

	@FXML
	private ImageView imgCheckin;


}
