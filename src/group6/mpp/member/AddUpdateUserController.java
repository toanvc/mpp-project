package group6.mpp.member;

import javafx.fxml.FXML;
import javafx.stage.Stage;

public class AddUpdateUserController {
	Stage primaryStage;
	private Stage dialogStage;
	private boolean isOkClicked;

	public void setPrimaryStage(Stage stage) {
		this.primaryStage = stage;
	}

	public void setDialogStage(Stage stage) {
		this.dialogStage = stage;
	}

	/**
	 * Returns true if the user clicked OK, false otherwise.
	 * 
	 * @return
	 */
	public boolean isOkClicked() {
		return isOkClicked;
	}

	@FXML
	private void onSaveMember() {
		isOkClicked = true;
		dialogStage.close();
	}

	@FXML
	private void onCancel() {
		dialogStage.close();

	}
}
