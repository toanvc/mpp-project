package group6.mpp.member;

import java.io.IOException;

import group6.mpp.MainApp;
import group6.mpp.objects.Member;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MembersController {
	Stage primaryStage;

	@FXML
	private Label txtIdMember;

	@FXML
	private void initialize() {
		txtIdMember.setText("diladsjfdkjs");
	}

	@FXML
	private void onActionNewMember() {
		showPersonEditDialog(null);
	}

	@FXML
	private void onActionUpdateMember() {

	}

	@FXML
	private void onActionDeleteMember() {

	}

	public void setPrimaryStage(Stage stage) {
		this.primaryStage = stage;
	}

	public boolean showPersonEditDialog(Member person) {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("member/addupdateuser.fxml"));
			Parent page =  loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edit Person");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(primaryStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);

			// Set the person into the controller.
			AddUpdateUserController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			// controller.setPerson(person);

			// Set the dialog icon.
			dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));

			// Show the dialog and wait until the user closes it
			dialogStage.showAndWait();

			return controller.isOkClicked();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
}
