package group6.mpp;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class MainApp extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	Stage secondWindow;
	Stage primaryStage;

	private void createSecondWindow() {
		secondWindow = new DashboardMainView(null, primaryStage);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		primaryStage.setTitle("Login");
		createSecondWindow();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(MainApp.class.getResource("login.fxml"));
		Parent loginView = loader.load();
		LoginController loginController = loader.getController();
		loginController.setMainApp(this);

		// create and position scene in stage
		Scene scene = new Scene(loginView, 700, 250);

		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public void showMainDashboard() {
		secondWindow.show();
		primaryStage.hide();
	}
	
	
}
