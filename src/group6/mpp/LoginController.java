package group6.mpp;

import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {

	@FXML
	private TextField inputId;
	@FXML
	private PasswordField inputPassword;

	MainApp mainApp;

	public LoginController() {
		// TODO Auto-generated constructor stub
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@FXML
	private void onActionLogin() {
		
		mainApp.showMainDashboard();
	}
}
