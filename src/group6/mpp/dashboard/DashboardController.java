package group6.mpp.dashboard;

import java.util.Calendar;
import java.util.Date;

import group6.mpp.objects.Book;
import group6.mpp.objects.BookBorrowHistory;
import group6.mpp.utils.MyUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class DashboardController {
	@FXML
	private TableView<BookBorrowHistory> tableStatus;
	@FXML
	private TableColumn<BookBorrowHistory, String> colMemberId;
	@FXML
	private TableColumn<BookBorrowHistory, String> colFrom;
	@FXML
	private TableColumn<BookBorrowHistory, String> colTo;
	@FXML
	private Text title;
	@FXML
	private Text author;

	@FXML
	private Text isbn;
	@FXML
	private Text copies;
	@FXML
	private Text availableCopies;
	@FXML
	private TextField inputSearch;

	public DashboardController() {

	}

	/**
	 * Initializes the controller class. This method is automatically called
	 * after the fxml file has been loaded.
	 */
	@FXML
	private void initialize() {
		// Initialize the person table with the two columns.
		colMemberId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().bookID));
		colFrom.setCellValueFactory(cellData -> MyUtils.convertDateToString(cellData.getValue().from));
		colTo.setCellValueFactory(cellData -> MyUtils.convertDateToString(cellData.getValue().to));
		title.setText("toan");

		// template
//		List<Author> list = new ArrayList<>();
//
//		for (int i = 0; i < 3; i++) {
//			list.add(new Author("Toan Vu " + i));
//		}
		//String iSBN, String title, String author, int copies, String image
		Book book = new Book("1234", "Java book", "Author", 1,"img");
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//		LocalDate date1 = LocalDate.parse("1989-05-05", formatter);
//		LocalDate date2 = LocalDate.parse("1989-05-07", formatter);
		Date date1 = Calendar.getInstance().getTime(); 
		date1.setDate(5);
		Date date2 = Calendar.getInstance().getTime();   
		checkOutEntries.add(new BookBorrowHistory("MemberId","BookId", date1, date2));
		tableStatus.setItems(checkOutEntries);

		setBook(book);

	}

	public void setBook(Book book) {
		this.title.setText(book.title);
		this.author.setText(book.author);
		this.isbn.setText(book.ISBN);
		this.copies.setText(String.valueOf(book.copies));
		this.availableCopies.setText("<depend on DB>");
	}

	/**
	 * Action field handler
	 */
	@FXML
	private void onActionSearch() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Search");
		alert.setHeaderText("Searching");
		String s = inputSearch.getText();
		alert.setContentText(s);
		alert.show();
	}

	@FXML
	private void onActionPrint() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Print");
		alert.setHeaderText("Print Click");
		alert.show();
	}

	@FXML
	private void onActionCheckout() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Checkout");
		alert.setHeaderText("Checkout Click :" + title.getText());
		alert.show();
	}

	/**
	 * Returns the data as an observable list of Persons.
	 * 
	 * @return
	 */
	public ObservableList<BookBorrowHistory> getPersonData() {
		return checkOutEntries;
	}

	ObservableList<BookBorrowHistory> checkOutEntries = FXCollections.observableArrayList();
}