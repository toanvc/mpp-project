package group6.mpp.objects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BookBorrowHistory implements java.io.Serializable {
	/**
	 * 
	 */
	public BookBorrowHistory(String member, String book, Date fromDate, Date toDate) {
		memberID = member;
		bookID = book;
		from = fromDate;
		to = toDate;
	}

	public BookBorrowHistory(String member, Date fromDate, Date toDate) {
		memberID = member;
		from = fromDate;
		to = toDate;
	}

	private static final long serialVersionUID = 7689242200964972279L;
	public Date from;
	public Date to;
	public String memberID;
	public String bookID;

	
	
}
