package group6.mpp.objects;

public class Main {

	void initializeUsers(Library myLibrary)
	{
		// define default users
		User systemAdmin = new User("001", "Admin", "123", UserTypes.SystemAdmin);
		myLibrary.addUser(systemAdmin);
			
	}
	
	void initializeMembers(Library myLibrary)
	{
		// define members
		Member mem1 = new Member("0001", "Ahmed Ali");
		Member mem2 = new Member("0002", "Ahmed Ali");
		Member mem3 = new Member("0003", "Ahmed Ali");
		myLibrary.addMember(mem1);
		myLibrary.addMember(mem2);
		myLibrary.addMember(mem3);
			
	}
	
	void initializeBooks(Library myLibrary)
	{
		// define books
		Book book1 = new Book("0001", "Java Core 1", "Paul Alen 1", 3, "");
		Book book2 = new Book("0002", "Java Core 2", "Paul Alen 2", 3, "");
		Book book3 = new Book("0003", "Java Core 3", "Paul Alen 3", 3, "");
		
		myLibrary.addBook(book1);
		myLibrary.addBook(book2);
		myLibrary.addBook(book3);
			
	}
	void initializeLibrary(Library myLibrary)
	{
		initializeUsers(myLibrary);
		initializeMembers(myLibrary);
		initializeBooks(myLibrary);
	}
	
	void operateLibrary(Library myLibrary)
	{
		myLibrary.checkOut("0001", "0001");	
		myLibrary.checkIn("0001");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Library myLibrary = Library.getInstance();

		System.out.println(myLibrary.members.size());
		
	}

}
