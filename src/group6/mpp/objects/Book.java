package group6.mpp.objects;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Hashtable;

import group6.mpp.objects.Book;

public class Book implements Serializable {
	public Book(String iSBN, String title, String author, int copies, String image) {
		super();
		ISBN = iSBN;
		this.title = title;
		this.author = author;
		this.copies = copies;
		this.image = image;

	}

	private static final long serialVersionUID = -5610938563964898630L;

	public String ISBN;
	public String title;
	public String author;
	public int copies;
	public int availableCopies;
	public String image;

	public int historyLength;

	public Hashtable<String, BookBorrowHistory> bookBorrowHistory;

	// Operations

	public void checkIn() {
		
		bookBorrowHistory.get(ISBN +"_"+ Integer.toString(historyLength)).to = Calendar.getInstance().getTime();
		this.availableCopies++;

	}

	public void checkOut(String memberID) {
		if(bookBorrowHistory == null)
			bookBorrowHistory = new Hashtable<String, BookBorrowHistory>();
		
		BookBorrowHistory borrowActivity = new BookBorrowHistory(memberID,Calendar.getInstance().getTime(), null);
		
		this.historyLength++;
		bookBorrowHistory.put(memberID + "_" + Integer.toString(historyLength), borrowActivity);
		this.availableCopies--;
	}
}
