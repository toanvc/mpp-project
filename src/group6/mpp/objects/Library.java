package group6.mpp.objects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.Hashtable;

public class Library implements java.io.Serializable {

	/**
	 * 
	 */
	private static Library thisInst;
	private static final long serialVersionUID = 5180686941200500224L;

	private Library() {
		super();

		// loadUsers();
		// loadBooks();
		// loadMembers();
	}

	public static Library getInstance() {

		if (thisInst == null)
			if (new File("myLibrary1.ser").exists()) {
				return Library.LoadLibrary();
			} else {
				return new Library();
			}

		return thisInst;
	}

	public final static int borrowingPeriod = 5; // Days
	public final static int maximumNumberofBorrowedBooks = 5;

	public Hashtable<String, Book> books = new Hashtable<String, Book>();
	public Hashtable<String, User> users = new Hashtable<String, User>();
	public Hashtable<String, Member> members = new Hashtable<String, Member>();

	// Main Library Operations
	public Book searchBook(String key) {
		return books.get(key);
	}

	public Member searchMember(String key) {
		return members.get(key);
	}

	public boolean checkOut(String bookID, String memberID) {

		Book thisBook = books.get(bookID);
		thisBook.checkOut(memberID);

		return true;

	}

	public void checkIn(String bookID) {

		books.get(bookID).checkIn();
	}

	// user Operations
	public Boolean addUser(User newUser) {
		users.put(newUser.ID, newUser);
		return true;
	}

	public Boolean updateUser(Member oldUser, Member newUser) {
		members.replace(oldUser.ID, oldUser, newUser);
		return true;
	}

	public Boolean deleteUser(String userID) {
		users.remove(userID);
		return true;
	}

	public User searchUser(String userID) {
		return users.get(userID);
	}
	// Member Operations
	public Boolean addMember(Member newMember) {
		members.put(newMember.ID, newMember);
		return true;
	}

	public Boolean updateMember(Member oldMember, Member newMember) {
		members.replace(oldMember.ID, oldMember, newMember);
		return true;
	}

	public Boolean deleteMember(String memberID) {
		members.remove(memberID);
		return true;
	}

	// Book Operations
	public Boolean addBook(Book book) {
		books.put(book.ISBN, book);
		return true;

	}

	public Boolean updateBook(Book oldBook, Book newBook) {
		books.replace(oldBook.ISBN, oldBook, newBook);
		return true;
	}

	public Boolean deleteBook(String bookID) {
		books.remove(bookID);
		return true;
	}

	@SuppressWarnings("resource")
	public static void StoreLibrary() {
		try {

			Files.deleteIfExists(new File("myLibrary1.ser").toPath());

			FileOutputStream f_out = new FileOutputStream("myLibrary1.ser");

			// Write object with ObjectOutputStream
			ObjectOutputStream obj_out = new ObjectOutputStream(f_out);

			// Write object out to disk
			obj_out.writeObject(thisInst);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Library LoadLibrary() {
		try {
			FileInputStream fileIn = new FileInputStream("myLibrary1.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			thisInst = (Library) in.readObject();
			in.close();
			fileIn.close();

			return thisInst;
		} catch (IOException i) {
			i.printStackTrace();
			return null;
		} catch (ClassNotFoundException c) {
			System.out.println("Employee class not found");
			c.printStackTrace();
			return null;
		}
	}
}
