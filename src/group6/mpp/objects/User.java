package group6.mpp.objects;

public class User  implements java.io.Serializable  {

	public User(String iD, String userName, String password, UserTypes role) {
		super();
		ID = iD;
		this.userName = userName;
		this.password = password;
		Role = role;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1045673856457207236L;
	public String ID;
	public String userName;
	public String password;
	public UserTypes Role;


}
