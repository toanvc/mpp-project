package group6.mpp;

import java.io.IOException;

import group6.mpp.book.BooksController;
import group6.mpp.dashboard.DashboardController;
import group6.mpp.member.MembersController;
import group6.mpp.objects.Member;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class DashboardMainView extends Stage {
	Member member;
	Stage loginStage;
	Stage primaryStage;

	public DashboardMainView(Member member, Stage loginStage) {
		this.member = member;
		this.loginStage = loginStage;
		init();
	}

	private final int DASHBOARD = 0;
	private final int BOOKS = 1;
	private final int MEMBERS = 2;
	private final int USER = 3;
	AnchorPane tabDashboard;
	AnchorPane tabBook;
	AnchorPane tabMember;
	AnchorPane wholeTab;
	AnchorPane tabUser;

	DashboardController dasboardControler;

	BooksController bookController;
	MembersController memberController;
	UserController userController;
	int currentTab = 0;

	private void init() {
		this.primaryStage = this;
		wholeTab = new AnchorPane();
		initTab();

		HBox panel = createPanelTabControl();

		// Use a border pane as the root for scene
		BorderPane border = new BorderPane();

		HBox hbox = addHBox();
		StackPane stack = new StackPane();

		stack.getChildren().addAll(wholeTab, panel);
		StackPane.setMargin(wholeTab, new Insets(70, 0, 0, 0));
		StackPane.setAlignment(panel, Pos.TOP_CENTER);
		border.setTop(hbox);
		// border.setLeft(addVBox());

		// Add a stack to the HBox in the top region
		addStackPane(hbox);
		// border.setRight(addFlowPane());
		// border.setRight(addTilePane());
		// border.setCenter(addAnchorPane(addGridPane()));
		border.setCenter(stack);
		border.setBottom(bottomPane());
		Scene scene = new Scene(border);
		this.setScene(scene);
		this.setTitle("Layout Sample");
		// this.show();
		this.setMaximized(true);
		// stage.setResizable(false);
	}

	private HBox addHBox() {

		HBox hbox = new HBox();
		hbox.setPadding(new Insets(0, 12, 0, 12));
		hbox.setSpacing(10); // Gap between nodes
		hbox.setStyle("-fx-background-color: #ffffff;");

		hbox.getChildren().addAll(bannerApp());

		return hbox;
	}

	private FlowPane bannerApp() {
		FlowPane flowPane = new FlowPane(Orientation.HORIZONTAL);

		FlowPane textPane = new FlowPane(Orientation.VERTICAL);
		textPane.setVgap(4);
		textPane.setHgap(4);
		textPane.setPrefWrapLength(130); // preferred width allows for two
											// columns
		textPane.setAlignment(Pos.CENTER);
		Text title = new Text("Library");
		title.setFont(Font.font("Arial", FontWeight.BOLD, 25));
		Text child = new Text("Management System");
		child.setFont(Font.font("Arial", FontWeight.NORMAL, 15));
		textPane.getChildren().addAll(title, child);
		ImageView imgBook = new ImageView(new Image("file:resource/books-icon-512.png"));// new
		imgBook.setFitHeight(100);
		imgBook.setFitWidth(100);
		flowPane.getChildren().addAll(imgBook, textPane);
		// flowPane.setMaxWidth(Double.MAX_VALUE);
		return flowPane;

	}

	private void addStackPane(HBox hb) {

		StackPane stack = new StackPane();
		GridPane grid = gridUserInfoPane("Toan Vu", "Admin");
		stack.getChildren().addAll(grid);
		stack.setAlignment(Pos.CENTER_RIGHT);
		// // Add offset to right for question mark to compensate for RIGHT
		// // alignment of all nodes
		StackPane.setMargin(grid, new Insets(0, 10, 0, 0));
		hb.getChildren().add(stack);
		HBox.setHgrow(stack, Priority.ALWAYS);
	}

	private GridPane gridUserInfoPane(String userName, String role) {

		GridPane grid = new GridPane();
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(0, 10, 0, 10));

		// Category in column 2, row 1
		Text userNameTitle = new Text("User name:");
		userNameTitle.setFont(Font.font("Arial", FontWeight.BOLD, 15));
		grid.add(userNameTitle, 0, 0);

		Text name = new Text(userName);
		name.setFont(Font.font("Arial", FontWeight.NORMAL, 15));
		grid.add(name, 1, 0);

		Text roleTitle = new Text("Role:");
		roleTitle.setFont(Font.font("Arial", FontWeight.BOLD, 15));
		grid.add(roleTitle, 0, 1);

		Text roleValue = new Text(role);
		roleValue.setFont(Font.font("Arial", FontWeight.NORMAL, 15));
		grid.add(roleValue, 1, 1);
		Image imageOk = new Image("file:resource/logout.png");// new
																// Image(getClass().getResourceAsStream("@/resource/logout.png"));
		Button button3 = new Button("   Logout         ", new ImageView(imageOk));
		grid.add(button3, 0, 2, 3, 1);
		grid.setAlignment(Pos.CENTER_RIGHT);
		return grid;
	}

	private TabPane createMainTabPane() {
		TabPane tabPane = new TabPane();

		// Create Tabs
		Tab tabA = new Tab();
		Button tabA_button = new Button("Button@Tab A");
		tabA.setContent(tabA_button);
		tabA.setClosable(false);
		tabPane.getTabs().add(tabA);

		Tab tabB = new Tab();
		// tabB.setText("Tab B");
		tabB.setClosable(false);
		// Add something in Tab
		StackPane tabB_stack = new StackPane();
		tabB_stack.setAlignment(Pos.CENTER);
		tabB_stack.getChildren().add(new Label("Label@Tab B"));
		tabB.setContent(tabB_stack);
		tabPane.getTabs().add(tabB);
		tabPane.getStylesheets().add(getClass().getResource("tab.css").toExternalForm());
		return tabPane;
	}

	private void initTab() {
		tabBook = new AnchorPane();
		tabDashboard = new AnchorPane();
		tabMember = new AnchorPane();
		tabUser = new AnchorPane();

		try {
			FXMLLoader loaderDashboard = new FXMLLoader();
			loaderDashboard.setLocation(MainApp.class.getResource("dashboard/Dashboard.fxml"));
			Parent dashboardView = loaderDashboard.load();
			dasboardControler = loaderDashboard.getController();

			tabDashboard.getChildren().add(dashboardView);

			setAnchorPaneMargin(dashboardView);

			// n.setStyle("-fx-background-color: #266699;");

			// Add book view to Tab
			FXMLLoader loaderBook = new FXMLLoader();
			loaderBook.setLocation(MainApp.class.getResource("book/book.fxml"));
			Parent bookView = loaderBook.load();
			bookController = loaderBook.getController();
			// Parent book =
			// FXMLLoader.load(getClass().getResource("book/book.fxml"));
			tabBook.getChildren().add(bookView);
			setAnchorPaneMargin(bookView);

			// Add user view to Tab
			FXMLLoader loaderUser = new FXMLLoader();
			loaderUser.setLocation(MainApp.class.getResource("member/users.fxml"));
			Parent userView = loaderUser.load();
			userController = loaderUser.getController();
			tabUser.getChildren().add(userView);
			setAnchorPaneMargin(userView);

			// Add user view to Tab
			FXMLLoader loadMember = new FXMLLoader();
			loadMember.setLocation(MainApp.class.getResource("member/member_.fxml"));
			Parent memberView = loadMember.load();
			memberController = loadMember.getController();
			memberController.setPrimaryStage(primaryStage);
			tabMember.getChildren().add(memberView);
			setAnchorPaneMargin(memberView);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		wholeTab.getChildren().addAll(tabBook, tabMember, tabDashboard, tabUser);

		// set margin anchor pane
		AnchorPane.setTopAnchor(tabBook, 0.0);
		AnchorPane.setRightAnchor(tabBook, 0.0);
		AnchorPane.setLeftAnchor(tabBook, 0.0);
		AnchorPane.setBottomAnchor(tabBook, 0.0);

		AnchorPane.setTopAnchor(tabMember, 0.0);
		AnchorPane.setRightAnchor(tabMember, 0.0);
		AnchorPane.setLeftAnchor(tabMember, 0.0);
		AnchorPane.setBottomAnchor(tabMember, 0.0);

		AnchorPane.setTopAnchor(tabDashboard, 0.0);
		AnchorPane.setRightAnchor(tabDashboard, 0.0);
		AnchorPane.setLeftAnchor(tabDashboard, 0.0);
		AnchorPane.setBottomAnchor(tabDashboard, 0.0);

		AnchorPane.setTopAnchor(tabUser, 0.0);
		AnchorPane.setRightAnchor(tabUser, 0.0);
		AnchorPane.setLeftAnchor(tabUser, 0.0);
		AnchorPane.setBottomAnchor(tabUser, 0.0);

		// init first state
		tabBook.setVisible(false);
		tabMember.setVisible(false);
		tabUser.setVisible(false);

	}

	private void setAnchorPaneMargin(Node node) {
		AnchorPane.setTopAnchor(node, 0.0);
		AnchorPane.setRightAnchor(node, 0.0);
		AnchorPane.setLeftAnchor(node, 0.0);
		AnchorPane.setBottomAnchor(node, 0.0);
	}

	private HBox createPanelTabControl() {
		HBox hbox = new HBox();
		hbox.setPadding(new Insets(15, 12, 15, 12));
		hbox.setMaxHeight(70);
		hbox.setSpacing(10); // Gap between nodes
		hbox.setStyle("-fx-background-color: #336699;");
		ImageView imgBook = new ImageView(new Image("file:resource/book library.png"));
		imgBook.setFitHeight(30);
		imgBook.setFitWidth(30);
		Button btnBook = new Button("BOOKS", imgBook);
		ImageView imgDashboard = new ImageView(new Image("file:resource/book library.png"));
		imgDashboard.setFitHeight(30);
		imgDashboard.setFitWidth(30);
		Button btnDashboard = new Button("Library Dashboard", imgDashboard);
		ImageView imgMember = new ImageView(new Image("file:resource/book library.png"));
		imgMember.setFitHeight(30);
		imgMember.setFitWidth(30);
		Button btnMembers = new Button("Members", imgMember);
		FlowPane rightPane = new FlowPane(Orientation.HORIZONTAL);

		rightPane.getChildren().addAll(btnBook, btnMembers);
		rightPane.setAlignment(Pos.CENTER_RIGHT);
		btnDashboard.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				onTabChange(DASHBOARD);
			}
		});
		btnBook.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				onTabChange(BOOKS);
			}
		});
		btnMembers.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				onTabChange(MEMBERS);
			}
		});
		hbox.getChildren().addAll(btnDashboard, rightPane);
		HBox.setHgrow(rightPane, Priority.ALWAYS);
		FlowPane.setMargin(btnMembers, new Insets(0, 30, 0, 30));
		return hbox;
	}

	private void onTabChange(int tabId) {
		if (tabId == currentTab) {
			return;
		}
		currentTab = tabId;
		switch (tabId) {
		case DASHBOARD:
			tabBook.setVisible(false);
			tabMember.setVisible(false);
			tabDashboard.setVisible(true);
			tabUser.setVisible(false);
			break;
		case BOOKS:
			tabBook.setVisible(true);
			tabMember.setVisible(false);
			tabDashboard.setVisible(false);
			tabUser.setVisible(false);
			break;
		case MEMBERS:
			tabBook.setVisible(false);
			tabMember.setVisible(true);
			tabDashboard.setVisible(false);
			tabUser.setVisible(false);
			break;
		case USER:
			tabBook.setVisible(false);
			tabMember.setVisible(false);
			tabDashboard.setVisible(false);
			tabUser.setVisible(true);
			break;
		default:
			tabBook.setVisible(false);
			tabMember.setVisible(false);
			tabDashboard.setVisible(true);
			tabUser.setVisible(false);
			break;
		}
	}

	private StackPane bottomPane() {
		StackPane bottomPane = new StackPane();

		ImageView imgUser = new ImageView(new Image("file:resource/user-group-icon.png"));
		imgUser.setFitHeight(30);
		imgUser.setFitWidth(30);
		Button btnUser = new Button("User", imgUser);
		bottomPane.setPadding(new Insets(15, 12, 15, 12));
		bottomPane.setStyle("-fx-background-color: #666699;");

		bottomPane.getChildren().add(btnUser);
		bottomPane.setAlignment(Pos.CENTER_LEFT);
		btnUser.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				onTabChange(USER);
			}
		});
		return bottomPane;
	}
}
