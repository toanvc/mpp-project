package group6.mpp.book;

import java.lang.reflect.Member;

import group6.mpp.objects.Book;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class BooksController {
	@FXML
	private TextField bookSearch;
	@FXML
	private TableView<Book> bookTable;
	@FXML
	private TableColumn<Book, String> isbnBookCol;
	@FXML
	private TableColumn<Book, String> nameBookCol;
	@FXML
	private TableColumn<Book, String> copyBookCol;
	@FXML
	private TableColumn<Book, String> authorBookCol;

	@FXML
	private GridPane bookGridPane;

	@FXML
	private TableView<Member> bookSatusTable;
	@FXML
	private TableColumn<Member,String> memberIDCol;

	@FXML
	private TableColumn<Member,String> fromCol;
	@FXML
	private TableColumn<Member,String> toCol;


	//Buttons

	@FXML
	private Button newBookbutton;

	@FXML
	private Button updateBookbutton;

	@FXML Button deleteBookbutton;

}
